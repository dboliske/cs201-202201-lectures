# CS 201

This repository will contain all examples written throughout the 2022 spring semester. There may not be much here at the start, but it will grow with time as it just has.

## Course Information

This course has additional resources and information available on the [course website](https://dboliske.github.io/IIT-Courses/#/201).

## Questions?

If you have any questions about the code presented here, please email me at [dboliske@hawk.iit.edu](dboliske@hawk.iit.edu) or message me on our [course Discord](https://discord.com/channels/922508817747550280/922508818397663284).