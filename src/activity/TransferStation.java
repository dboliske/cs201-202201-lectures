package activity;

import java.util.ArrayList;

public class TransferStation extends Station {
	
	private ArrayList<Line> transfers;
	
	public TransferStation() {
		super();
		transfers = new ArrayList<Line>();
	}
	
	public TransferStation(String name, double lat, double lng, String description, boolean wheelchair) {
		super(name, lat, lng, description, wheelchair);
		transfers = new ArrayList<Line>();
	}
	
	public TransferStation(String name, double lat, double lng, String description, boolean wheelchair, ArrayList<Line> transfers) {
		super(name, lat, lng, description, wheelchair);
		this.transfers = new ArrayList<Line>(transfers.size());
		for (Line l : transfers) {
			this.transfers.add(l);
		}
	}
	
	public void addTransfer(Line l) {
		if (!transfers.contains(l)) {
			transfers.add(l);
		}
	}
	
	public void removeTransfer(Line l) {
		transfers.remove(l);
	}
	
	public void removeAllTransfers() {
		transfers.clear();
	}
	
	public int transfers() {
		return transfers.size();
	}
	
	public Line get(int i) {
		return transfers.get(i);
	}
	
	public boolean canTransfer() {
		return true;
	}
	
	public boolean transferTo(Line line) {
		return transfers.contains(line);
	}
	
	public String toString() {
		String result = super.toString();
		result += "\nTransfers: ";
		if (transfers.size() == 0) {
			result += "NONE";
		} else {
			result += transfers.get(0).getName();
			for (int i=1; i<transfers.size(); i++) {
				result += ", " + transfers.get(i).getName();
			}
		}
		
		return result;
	}
	
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		} else if (this == obj) {
			return true;
		} else if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof TransferStation)) {
			return false;
		}
		
		TransferStation t = (TransferStation)obj;
		
		if (transfers.size() != t.transfers()) {
			return false;
		}
		
		for (int i=0; i<transfers.size(); i++) {
			if (transfers.get(i) == null && t.get(i) != null) {
				return false;
			} else if (transfers.get(i) != null && t.get(i) == null) {
				return false;
			} else if (transfers.get(i) != null && !transfers.get(i).equals(t.get(i))) {
				return false;
			}
		}
		
		return true;
	}

}
