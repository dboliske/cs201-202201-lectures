package activity;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class CTAApp {
	
	public static ArrayList<Line> readData(String filename) {
		ArrayList<Line> cta = new ArrayList<Line>();
		
		try {
			File f = new File(filename);
			Scanner in = new Scanner(f);
			
			// construct lines
			String[] nameSizes = in.nextLine().split(",");
			String[] loopInfo = in.nextLine().split(",");
			for (int i=5; i<nameSizes.length; i++) {
				String[] nameSizeInfo = nameSizes[i].split(":");
				int loopIndex = Integer.parseInt(loopInfo[i]);
				Line l;
				if (loopIndex >= 0) {
					l = new LoopLine(nameSizeInfo[0], Integer.parseInt(nameSizeInfo[1]), loopIndex);
				} else {
					l = new Line(nameSizeInfo[0], Integer.parseInt(nameSizeInfo[1]));
				}
				
				cta.add(l);
			}
			// construct stations
			while (in.hasNextLine()) {
				try {
					String[] stationInfo = in.nextLine().split(",");
					String name = stationInfo[0];
					double lat = Double.parseDouble(stationInfo[1]);
					double lng = Double.parseDouble(stationInfo[2]);
					String description = stationInfo[3];
					boolean wheelchair = Boolean.parseBoolean(stationInfo[4]);
					int[] ids = new int[stationInfo.length - 5];
					boolean transfer = false;
					boolean foundLine = false;
					for (int i=0; i<ids.length; i++) {
						ids[i] = Integer.parseInt(stationInfo[i + 5]);
						if (ids[i] >= 0) {
							if (foundLine) {
								transfer = true;
							}
							foundLine = true;
						}
					}
					
					Station s;
					if (transfer) {
						ArrayList<Line> transfers = new ArrayList<Line>();
						for (int i=0; i<ids.length; i++) {
							if (ids[i] >= 0) {
								transfers.add(cta.get(i));
							}
						}
						s = new TransferStation(name, lat, lng, description, wheelchair, transfers);
					} else {
						s = new Station(name, lat, lng, description, wheelchair);
					}
					
					for (int i=0; i<ids.length; i++) {
						if (ids[i] >= 0) {
							cta.get(i).setStation(s, ids[i]);
						}
					}
				} catch (Exception e) {}
			}
			
			in.close();
		} catch (Exception e) {
			System.out.println("Error reading in data file.");
		}
		
		return cta;
	}
	
	public static ArrayList<Line> menu(ArrayList<Line> cta) {
		Scanner input = new Scanner(System.in);
		
		boolean done = false;
		
		do {
			// Print out menu
			System.out.println("1. Create Station");
			System.out.println("2. Modify Station");
			System.out.println("3. Delete Station");
			System.out.println("4. Find Station");
			System.out.println("5. Find Route");
			System.out.println("6. Exit");
			// Get user selection
			System.out.print("Choice: ");
			String in = input.nextLine();
			// Handle user selection
			switch (in) {
				case "1": // create
					cta = createStation(cta, input);
					break;
				case "2": // edit
					cta = modifyStation(cta, input);
					break;
				case "3": // delete
					cta = deleteStation(cta, input);
					break;
				case "4": // find
					Station s = findStation(cta, input);
					if (s == null) {
						System.out.println("No station found");
					} else {
						System.out.println(s.toString());
					}
					break;
				case "5": // route
					findRoute(cta, input);
					break;
				case "6": // exit
					done = true;
					break;
				default:
					System.out.println("I'm sorry, but I didn't understand that. Please try again");
			}
		} while (!done);
		
		System.out.println("Goodbye!");
		
		return cta;
	}
	
	public static ArrayList<Line> createStation(ArrayList<Line> cta, Scanner input) {
		System.out.print("Name: ");
		String name = input.nextLine();
		double lat = readDouble(input, "Latitude: ", Location.minLatitude, Location.maxLatitude);
		double lng = readDouble(input, "Longitude: ", Location.minLongitude, Location.maxLongitude);
		System.out.print("Description: ");
		String description = input.nextLine();
		boolean wheelchair = readBoolean(input, "Wheelchair Access: ");
		int[] ids = new int[cta.size()];
		boolean transfer = false;
		boolean foundLine = false;
		for (int i=0; i<ids.length; i++) {
			boolean onLine = readBoolean(input, "Is "+name+" on the "+cta.get(i).getName()+" line? ");
			if (onLine) {
				if (foundLine) {
					transfer = true;
				}
				foundLine = true;
				
				// find out where on the line
				ids[i] = readInteger(input, "Station Index: ", 0, cta.get(i).size());
			} else {
				ids[i] = -1;
			}
		}
		
		Station s;
		if (transfer) {
			ArrayList<Line> transfers = new ArrayList<Line>();
			for (int i=0; i<ids.length; i++) {
				if (ids[i] >= 0) {
					transfers.add(cta.get(i));
				}
			}
			s = new TransferStation(name, lat, lng, description, wheelchair, transfers);
		} else {
			s = new Station(name, lat, lng, description, wheelchair);
		}
		
		for (int i=0; i<ids.length; i++) {
			if (ids[i] >= 0) {
				cta.get(i).addStation(s, ids[i]);
			}
		}
		
		return cta;
	}
	
	public static double readDouble(Scanner input, String prompt, double min, double max) {
		double value = 0.0;
		boolean done = false;
		do {
			System.out.print(prompt);
			String in = input.nextLine();
			try {
				value = Double.parseDouble(in);
				if (value >= min && value <= max) {
					done = true;
				} else {
					System.out.println(value + " is outside the valid range.");
				}
			} catch (Exception e) {
				System.out.println("'" + in + "' is not a valid number.");
			}
		} while (!done);
		
		return value;
	}
	
	public static boolean readBoolean(Scanner input, String prompt) {
		boolean value = false;
		boolean done = false;
		do {
			System.out.print(prompt);
			String in = input.nextLine();
			switch (in.toLowerCase()) {
				case "y": case "yes": case "t": case "true":
					value = true;
					done = true;
					break;
				case "n": case "no": case "f": case "false":
					value = false;
					done = true;
					break;
				default:
					System.out.println("Please enter 'yes' or 'no'.");
			}
		} while (!done);
		
		return value;
	}
	
	public static int readInteger(Scanner input, String prompt, int min, int max) {
		int value = 0;
		boolean done = false;
		do {
			System.out.print(prompt);
			String in = input.nextLine();
			try {
				value = Integer.parseInt(in);
				if (value >= min && value <= max) {
					done = true;
				} else {
					System.out.println(value + " is outside the valid range.");
				}
			} catch (Exception e) {
				System.out.println("'" + in + "' is not a valid number.");
			}
		} while (!done);
		
		return value;
	}
	
	public static ArrayList<Line> modifyStation(ArrayList<Line> cta, Scanner input) {
		Station s = findStation(cta, input);
		boolean done = false;
		do {
			System.out.println("Which to edit?");
			System.out.println("1. Name");
			System.out.println("2. Location");
			System.out.println("3. Description");
			System.out.println("4. Wheelchair Access");
			System.out.println("5. Transfers/Location on Lines");
			System.out.println("6. Exit");
			System.out.print("Choice: ");
			String in = input.nextLine();
			switch (in) {
				case "1":
					System.out.println("Current Name: " + s.getName());
					System.out.print("Name: ");
					s.setName(input.nextLine());
					break;
				case "2":
					System.out.println("Current Location: " + s.getLocationString());
					s.setLatitude(readDouble(input, "Latitude: ", Location.minLatitude, Location.maxLatitude));
					s.setLongitude(readDouble(input, "Longitude: ", Location.minLongitude, Location.maxLongitude));
					break;
				case "3":
					System.out.println("Current Description: " + s.getDescription());
					System.out.print("Description: ");
					s.setDescription(input.nextLine());
					break;
				case "4":
					System.out.println("Current Access: " + (s.hasWheelchairAccess()?"Yes":"No"));
					s.setWheelchairAccess(readBoolean(input, "Wheelchair Access: "));
					break;
				case "5":
					int[] ids = new int[cta.size()];
					boolean transfer = false;
					boolean foundLine = false;
					for (int i=0; i<ids.length; i++) {
						System.out.println("Currently on " + cta.get(i).getName() + " Line: " + (cta.get(i).contains(s)?"Yes":"No"));
						boolean onLine = readBoolean(input, "Is "+s.getName()+" still on the "+cta.get(i).getName()+" line? ");
						if (onLine) {
							if (foundLine) {
								transfer = true;
							}
							foundLine = true;
							
							// find out where on the line
							ids[i] = readInteger(input, "Station Index: ", 0, cta.get(i).size());
						} else {
							ids[i] = -1;
						}
					}
					for (Line l : cta) {
						l.removeStation(s);
					}
					Station replacement = s;
					if (s instanceof TransferStation && !transfer) {
						replacement = new Station(s.getName(), s.getLatitude(), s.getLongitude(), s.getDescription(), s.hasWheelchairAccess());
					} else if (s instanceof TransferStation && transfer) {
						((TransferStation)(replacement)).removeAllTransfers();
					} else if (transfer) {
						ArrayList<Line> transfers = new ArrayList<Line>();
						for (int i=0; i<ids.length; i++) {
							if (ids[i] >= 0) {
								transfers.add(cta.get(i));
							}
						}
						replacement = new TransferStation(s.getName(), s.getLatitude(), s.getLongitude(), s.getDescription(), s.hasWheelchairAccess(), transfers);
					}
					
					for (int i=0; i<ids.length; i++) {
						if (ids[i] != -1) {
							cta.get(i).addStation(replacement, ids[i]);
						}
					}
					break;
				case "6":
					done = true;
					break;
				default:
					System.out.println("Not an option");
			}
		} while (!done);
		
		return cta;
	}
	
	public static ArrayList<Line> deleteStation(ArrayList<Line> cta, Scanner input) {
		Station toRemove = findStation(cta, input);
		if (toRemove == null) {
			System.out.println("No station to remove.");
			return cta;
		}
		
		for (Line l : cta) {
			l.removeStation(toRemove);
		}
		return cta;
	}
	
	public static Station findStation(ArrayList<Line> cta, Scanner input) {
		System.out.print("Find station by name or nearest: ");
		String in = input.nextLine();
		switch (in) {
			case "name":
				return findNamedStation(cta, input);
			case "nearest":
				return findNearestStation(cta, input);
			default:
				System.out.println("That is not an option. Returning to menu.");
		}
		return null;
	}
	
	public static Station findNamedStation(ArrayList<Line> cta, Scanner input) {
		System.out.print("Name: ");
		String name = input.nextLine();
		ArrayList<Station> possible = new ArrayList<Station>();
		for (Line l : cta) {
			for (int i=0; i<l.size(); i++) {
				Station s = l.getStation(i);
				if (!possible.contains(s) && s.getName().equalsIgnoreCase(name)) {
					possible.add(s);
				}
			}
		}
		
		if (possible.size() == 0) {
			return null;
		} else if (possible.size() == 1) {
			return possible.get(0);
		}
		
		System.out.println("Which station did you mean?");
		for (int i=0; i<possible.size(); i++) {
			System.out.println((i+1) + possible.get(i).toString());
		}
		int index = readInteger(input, "Choice: ", 1, possible.size()) - 1;
		return possible.get(index);
	}
	
	public static Station findNearestStation(ArrayList<Line> cta, Scanner input) {
		double lat = readDouble(input, "Latitude: ", Location.minLatitude, Location.maxLatitude);
		double lng = readDouble(input, "Longitude: ", Location.minLongitude, Location.maxLongitude);
		
		Location user = new Location(lat, lng);
		
		boolean cantFind = cta.size() == 0;
		for (int i=0; !cantFind&&i<cta.size(); i++) {
			cantFind = cta.get(i).size() == 0;
		}
		
		if (cantFind) {
			return null;
		}
		
		Station nearest = cta.get(0).getStation(0);
		double distance = user.calcDistance(nearest);
		for (Line l : cta) {
			for (int i=0; i<l.size(); i++) {
				double test = user.calcDistance(l.getStation(i));
				if (test < distance) {
					distance = test;
					nearest = l.getStation(i);
				}
			}
		}
		
		return nearest;
	}
	
	public static void findRoute(ArrayList<Line> cta, Scanner input) {
		Station start = findStation(cta, input);
		Station end = findStation(cta, input);
		Line routeLine = findBestNoTransferRoute(cta, start, end);
		
		if (routeLine != null) {
			printRoute(routeLine, start, end, "Board the " + routeLine.getName() + " Line");
		} else {
		
			Line startLine = findLine(cta, start);
			Line endLine = findLine(cta, end);
			
			Station transfer = startLine.findTransfer(endLine);
			if (transfer == null) {
				System.out.println("Route not possible");
			} else {
				Line bestStartLine = findBestNoTransferRoute(cta, start, transfer);
				printRoute(bestStartLine, start, transfer, "Board the " + bestStartLine.getName() + " Line");
				Line bestEndLine = findBestNoTransferRoute(cta, transfer, end);
				printRoute(bestEndLine, transfer, end, "Transfer to the " + bestEndLine.getName() + " Line");
			}
		}
	}
	
	public static Line findBestNoTransferRoute(ArrayList<Line> cta, Station start, Station end) {
		ArrayList<Station> route = null;
		Line routeLine = null;
		for (Line l : cta) {
			ArrayList<Station> possibleRoute = l.findRoute(start, end);
			if (possibleRoute != null) {
				if (route == null || route.size() > possibleRoute.size()) {
					route = possibleRoute;
					routeLine = l;
				}
			}
		}
		
		return routeLine;
	}
	
	public static void printRoute(Line l, Station start, Station end, String text) {
		ArrayList<Station> route = l.findRoute(start, end);
		System.out.println(text);
		for (Station s : route) {
			System.out.println(s.getName());
		}
	}
	
	public static Line findLine(ArrayList<Line> cta, Station s) {
		for (int i=0; i<cta.size(); i++) {
			if (cta.get(i).contains(s)) {
				return cta.get(i);
			}
		}
		
		return null;
	}

	public static void main(String[] args) {
		// Call method to read in system details
		ArrayList<Line> cta = readData("src/activity/CTAStops.csv");
		// Call method to handle user menu
		cta = menu(cta);
		// Call method to save details (optional)
	}

}
