# Lecture Activity

## The Problem

Design a system to model the CTA.

### User Interface

1. Create a new station
2. Delete a station
3. Modify a station (edit)
4. Search for a station (by name)
5. Find nearest station (by user location)
6. Find route between stations (either specified by name or user location)
7. Exit (save changes to system out to a file)
