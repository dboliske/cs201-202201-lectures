package activity;

import java.util.ArrayList;

public class LoopLine extends Line {
	
	private int loopIndex;
	
	public LoopLine() {
		super();
		loopIndex = -1;
	}
	
	public LoopLine(String name, int count) {
		super(name, count);
		loopIndex = -1;
	}
	
	public LoopLine(String name, int count, int lIndex) {
		super(name, count);
		setLoopIndex(lIndex);
	}
	
	public LoopLine(String name, Station[] stops) {
		super(name, stops);
		loopIndex = -1;
	}
	
	public LoopLine(String name, ArrayList<Station> stops) {
		super(name, stops);
		loopIndex = -1;
	}
	
	public LoopLine(String name, Station[] stops, int lIndex) {
		this(name, stops);
		setLoopIndex(lIndex);
	}
	
	public LoopLine(String name, ArrayList<Station> stops, int lIndex) {
		this(name, stops);
		setLoopIndex(lIndex);
	}

	
	public void addStation(Station stop, int index) {
		if (!super.contains(stop)) {
			super.addStation(stop, index);
			
			if (index <= loopIndex) {
				loopIndex = loopIndex + 1;
			}
		}
	}
	
	public Station getLoopStation() {
		return super.getStation(loopIndex);
	}
	
	public int getLoopIndex() {
		return loopIndex;
	}

	public void setLoopIndex(int lIndex) {
		if (lIndex >= 0 && lIndex < super.size()) {
			this.loopIndex = lIndex;
		}
	}
	
	public String toString() {
		String result = name + "Line\n";
		for (int i=0; i<size(); i++) {
			Station stop = getStation(i);
			if (stop != null) {
				result += "\n" + stop.getName();
			}
			if (i == loopIndex) {
				result += " - Loops Here";
			}
		}
		
		return result;
	}
	
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof LoopLine)) {
			return false;
		}
		
		LoopLine ll = (LoopLine) obj;
		return loopIndex == ll.getLoopIndex();
	}
	
	public ArrayList<Station> findRoute(Station start, Station end) {
		ArrayList<Station> results = null;
		if (contains(start) && contains(end)) {
			// both on this line, we can find a path
			results = new ArrayList<Station>();
			int indexOfStart = indexOf(start);
			int indexOfEnd = indexOf(end);
			// Both outside the loop
			if (indexOfStart <= loopIndex && indexOfEnd <= loopIndex) {
				if (indexOfStart <= indexOfEnd) {
					for (int i=indexOfStart; i<=indexOfEnd; i++) {
						results.add(getStation(i));
					}
				} else if (indexOfStart > indexOfEnd) {
					for (int i=indexOfStart; i>=indexOfEnd; i--) {
						results.add(getStation(i));
					}
				}
			// Only start outside the loop
			} else if (indexOfStart <= loopIndex) {
				for (int i=indexOfStart; i<=indexOfEnd; i++) {
					results.add(getStation(i));
				}
			// Only end outside the loop
			} else if (indexOfEnd <= loopIndex) {
				for (int i=indexOfStart; i<size(); i++) {
					results.add(getStation(i));
				}
				for (int i=loopIndex; i>=indexOfEnd; i--) {
					results.add(getStation(i));
				}
			// Both in the loop, but start before end
			} else if (indexOfStart <= indexOfEnd) {
				for (int i=indexOfStart; i<=indexOfEnd; i++) {
					results.add(getStation(i));
				}
			}
			// If both in the loop, but end before start, not possible
		}
		
		return results;
	}

}
