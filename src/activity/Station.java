package activity;

public class Station extends Location {
	
	protected String name;
	private String description;
	private boolean wheelchairAccess;
	
	public Station() {
		super();
		name = "Station";
		description = "elevated";
		wheelchairAccess = false;
	}
	
	public Station(String name, double lat, double lng, String description, boolean wheelchair) {
		super(lat, lng);
		this.name = name;
		setDescription(description);
		wheelchairAccess = wheelchair;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean hasWheelchairAccess() {
		return wheelchairAccess;
	}

	public void setWheelchairAccess(boolean wheelchairAccess) {
		this.wheelchairAccess = wheelchairAccess;
	}
	
	public String getLocationString() {
		return super.toString();
	}
	
	public String toString() {
		String result = "Name: " + name;
		result += "\nLocation: " + super.toString();
		result += "\nDescription: " + description;
		result += "\nWheelchair Access: " + (wheelchairAccess?"Yes":"No");
		
		return result;
	}
	
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		} else if (this == obj) {
			return true;
		} else if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof Station)) {
			return false;
		}
		
		Station s = (Station) obj;
		if (!name.equals(s.getName())) {
			return false;
		} else if (!description.equals(s.getDescription())) {
			return false;
		} else if (wheelchairAccess != s.hasWheelchairAccess()) {
			return false;
		}
		
		return true;
	}
	
	public boolean canTransfer() {
		return false;
	}
	
	public boolean transferTo(Line line) {
		return false;
	}

}
