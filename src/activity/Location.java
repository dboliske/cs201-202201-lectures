package activity;

public class Location {

	private double latitude;
	private double longitude;
	
	public static double minLatitude = -90;
	public static double maxLatitude = 90;
	public static double minLongitude = -180;
	public static double maxLongitude = 180;
	public static double latTolerance = 0.0001;
	public static double lngTolerance = 0.0001;
	
	public Location() {
		latitude = 0.0;
		longitude = 0.0;
	}
	
	public Location(double lat, double lng) {
		this();
		setLatitude(lat);
		setLongitude(lng);
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		if (latitude >= minLatitude && latitude <= maxLatitude) {
			this.latitude = latitude;
		}
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		if (longitude >= minLongitude && longitude <= maxLongitude) {
			this.longitude = longitude;
		}
	}
	
	public String toString() {
		return "(" + latitude + ", " + longitude + ")";
	}
	
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		} else if (this == obj) {
			return true;
		} else if (!(obj instanceof Location)) {
			return false;
		}
		
		Location l = (Location)obj;
		if (Math.abs(l.getLatitude() - this.latitude) > latTolerance) {
			return false;
		} else if (Math.abs(l.getLongitude() - this.longitude) > lngTolerance) {
			return false;
		}
		
		return true;
	}
	
	public double calcDistance(Location loc) {
		return Math.sqrt(Math.pow(loc.getLatitude() - this.latitude, 2) + Math.pow(loc.getLongitude() - this.longitude, 2));
	}
	
}
