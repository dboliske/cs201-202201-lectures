package activity;

import java.util.ArrayList;

public class Line {
	
	protected String name;
	private ArrayList<Station> stops;
	
	public Line() {
		name = "Line";
		stops = new ArrayList<Station>();
	}
	
	public Line(String name, int count) {
		this.name = name;
		stops = new ArrayList<Station>(count);
		for (int i=0; i<count; i++) {
			stops.add(null);
		}
	}
	
	public Line(String name, Station[] stops) {
		this.name = name;
		this.stops = new ArrayList<Station>(stops.length);
		for (Station s : stops) {
			this.stops.add(s);
		}
	}
	
	public Line(String name, ArrayList<Station> stops) {
		this.name = name;
		this.stops = new ArrayList<Station>(stops.size());
		for (Station s : stops) {
			this.stops.add(s);
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void addStation(Station stop) {
		if (!stops.contains(stop)) {
			stops.add(stop);
		}
	}
	
	public void addStation(Station stop, int index) {
		if (!stops.contains(stop)) {
			stops.add(index, stop);
		}
	}
	
	public void setStation(Station stop, int index) {
		if (stops.contains(stop)) {
			stops.remove(stop);
		}
		
		stops.set(index, stop);
	}
	
	public void removeStation(Station stop) {
		stops.remove(stop);
	}
	
	public Station getStation(int index) {
		return stops.get(index);
	}
	
	public boolean contains(Station s) {
		return stops.contains(s);
	}
	
	public int indexOf(Station s) {
		return stops.indexOf(s);
	}
	
	public int size() {
		return stops.size();
	}
	
	public String toString() {
		String result = name + " Line\n";
		for (Station stop : stops) {
			if (stop != null) {
				result += "\n" + stop.getName();
			}
		}
		
		return result;
	}
	
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		} else if (this == obj) {
			return true;
		} else if (!(obj instanceof Line)) {
			return false;
		}
		
		Line l = (Line) obj;
		if (!name.equals(l.getName())) {
			return false;
		} else if (stops.size() != l.size()) {
			return false;
		}
		
		for (int i=0; i<stops.size(); i++) {
			if (stops.get(i) == null && l.getStation(i) != null) {
				return false;
			} else if (stops.get(i) != null && l.getStation(i) == null) {
				return false;
			} else if (stops.get(i) != null && !stops.get(i).equals(l.getStation(i))) {
				return false;
			}
		}
		
		return true;
	}
	
	public ArrayList<Station> findRoute(Station start, Station end) {
		ArrayList<Station> results = null;
		if (stops.contains(start) && stops.contains(end)) {
			// both on this line, we can find a path
			results = new ArrayList<Station>();
			int indexOfStart = stops.indexOf(start);
			int indexOfEnd = stops.indexOf(end);
			if (indexOfStart < indexOfEnd) {
				for (int i=indexOfStart; i<=indexOfEnd; i++) {
					results.add(stops.get(i));
				}
			} else if (indexOfStart > indexOfEnd) {
				for (int i=indexOfStart; i>=indexOfEnd; i--) {
					results.add(stops.get(i));
				}
			} else {
				results.add(start);
			}
		}
		
		return results;
	}
	
	public Station findTransfer(Line l) {
		for (Station s : stops) {
			if (s instanceof TransferStation) {
				TransferStation transfer = (TransferStation)s;
				if (transfer.transferTo(l)) {
					return transfer;
				}
			}
		}
		
		return null;
	}

}
