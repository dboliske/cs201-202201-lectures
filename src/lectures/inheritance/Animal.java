package lectures.inheritance;

public class Animal {

	private String name;
	private int age;
	private double weight;
	
	public Animal() {
		this.name = "Spot";
		this.age = 1;
		this.weight = 1.0;
	}
	
	public Animal(String name, int age, double weight) {
		this.name = name;
		this.age = 1;
		setAge(age);
		this.weight = 1.0;
		setWeight(weight);
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setAge(int age) {
		if (age >= 0) {
			this.age = age;
		}
	}
	
	public void setWeight(double weight) {
		if (weight > 0) {
			this.weight = weight;
		}
	}
	
	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;
	}
	
	public double getWeight() {
		return weight;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Animal)) {
			return false;
		}
		
		Animal a = (Animal)obj;
		if (!this.name.equals(a.getName())) {
			return false;
		} else if (this.age != a.getAge()) {
			return false;
		} else if (this.weight != a.getWeight()) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String toString() {
		return name + " is " + age + " years old and is " + weight + " lbs.";
	}
	
	protected String csvData() {
		return name + "," + age + "," + weight;
	}
	
	public String toCSV() {
		return "Animal," + csvData();
	}
}
