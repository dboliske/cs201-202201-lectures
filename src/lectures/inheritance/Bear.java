package lectures.inheritance;

public class Bear extends Animal {

	private boolean hibernating;
	
	public Bear() {
		super();
		hibernating = false;
	}
	
	public Bear(String name, int age, double weight, boolean hibernate) {
		super(name, age, weight);
		this.hibernating = hibernate;
	}
	
	public void setHibernating(boolean hibernate) {
		hibernating = hibernate;
	}
	
	public boolean isHibernating() {
		return hibernating;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof Bear)) {
			return false;
		}
		
		Bear b = (Bear)obj;
		if (this.hibernating != b.isHibernating()) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String toString() {
		return super.toString() + " " + super.getName() + " is a bear.";
	}
	
	@Override
	public String toCSV() {
		return "Bear," + super.csvData() + "," + hibernating;
	}
	
}
