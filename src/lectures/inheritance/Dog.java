package lectures.inheritance;

public class Dog extends Animal {

	private boolean goodBoy;
	
	public Dog() {
		super();
		goodBoy = true;
	}
	
	public Dog(String name, int age, double weight, boolean goodBoy) {
		super(name, age, weight);
		this.goodBoy = goodBoy;
	}
	
	public void setGoodBoy(boolean goodBoy) {
		this.goodBoy = goodBoy;
	}
	
	public boolean getGoodBoy() {
		return true;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof Dog)) {
			return false;
		}
		
		Dog d = (Dog)obj;
		if (!this.goodBoy == d.getGoodBoy()) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String toString() {
		return super.toString() + " " + super.getName() + " is a dog.";
	}
	
}
