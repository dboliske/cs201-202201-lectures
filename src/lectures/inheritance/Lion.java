package lectures.inheritance;

public class Lion extends Animal {

	private boolean feed;
	
	public Lion() {
		super();
		feed = false;
	}
	
	public Lion(String name, int age, double weight, boolean feed) {
		super(name, age, weight);
		this.feed = feed;
	}
	
	public void setFeed(boolean feed) {
		this.feed = feed;
	}
	
	public boolean hasFeed() {
		return feed;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof Lion)) {
			return false;
		}
		
		Lion l = (Lion)obj;
		if (this.feed != l.hasFeed()) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String toString() {
		return super.toString() + " " + super.getName() + " is a lion.";
	}
	
	@Override
	public String toCSV() {
		return "Lion," + super.csvData() + "," + feed;
	}
	
}
