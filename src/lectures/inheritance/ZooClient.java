package lectures.inheritance;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Scanner;

public class ZooClient {
	
	public static Animal[] readFile(String filename) {
		Animal[] animals = new Animal[10];
		int count = 0;
		
		try {
			File f = new File(filename);
			Scanner input = new Scanner(f);
			
			// end-of-file loop
			while (input.hasNextLine()) {
				try {
					String line = input.nextLine();
					String[] values = line.split(",");
					Animal a = null;
					switch (values[0].toLowerCase()) {
						case "elephant":
							a = new Elephant(
								values[1],
								Integer.parseInt(values[2]),
								Double.parseDouble(values[3])
							);
							break;
						case "lion":
							a = new Lion(
									values[1],
									Integer.parseInt(values[2]),
									Double.parseDouble(values[3]),
									Boolean.parseBoolean(values[4])
								);
							break;
						case "bear":
							a = new Bear(
									values[1],
									Integer.parseInt(values[2]),
									Double.parseDouble(values[3]),
									Boolean.parseBoolean(values[4])
								);
							break;
					}
					
					if (animals.length == count) {
						animals = resize(animals, animals.length*2);
					}
					
					animals[count] = a;
					count++;
				} catch (Exception e) {
					
				}
			}
			
			input.close();
		} catch (FileNotFoundException fnf) {
			// Do nothing.
		} catch(Exception e) {
			System.out.println("Error occurred reading in file.");
		}
		
		animals = resize(animals, count);
		
		return animals;
	}
	
	public static Animal[] resize(Animal[] data, int size) {
		Animal[] temp = new Animal[size];
		int limit = data.length > size ? size : data.length;
		for (int i=0; i<limit; i++) {
			temp[i] = data[i];
		}
		
		return temp;
	}
	
	public static Animal[] menu(Scanner input, Animal[] data) {
		boolean done = false;
		
		do {
			System.out.println("1. Purchase Animal");
			System.out.println("2. Sell Animal");
			System.out.println("3. List Animals");
			System.out.println("4. Exit");
			System.out.print("Choice: ");
			String choice = input.nextLine();
			
			switch (choice) {
				case "1": // Create new animal
					data = createAnimal(data, input);
					break;
				case "2": // Remove existing animal
					data = removeAnimal(data, input);
					break;
				case "3": // List animals
					listAnimals(data);
					break;
				case "4": // Exit
					done = true;
					break;
				default:
					System.out.println("I'm sorry, but I didn't understand that.");
			}
		} while (!done);
		
		return data;
	}
	
	public static Animal[] createAnimal(Animal[] data, Scanner input) {
		
		Animal a;
		System.out.print("Animal Name: ");
		String name = input.nextLine();
		System.out.print("Animal Age: ");
		int age = 0;
		try {
			age = Integer.parseInt(input.nextLine());
		} catch (Exception e) {
			System.out.println("That is not a valid age. Returning to menu.");
			return data;
		}
		System.out.print("Animal Weight: ");
		double weight = 0;
		try {
			weight = Double.parseDouble(input.nextLine());
		} catch (Exception e) {
			System.out.println("That is not a valid weight. Returning to menu.");
			return data;
		}
		
		System.out.print("Type of Animal (Lion, Bear, or Elephant): ");
		String type = input.nextLine();
		switch (type.toLowerCase()) {
			case "lion":
				System.out.print("Lion fed (y/n): ");
				boolean fed = false;
				String yn = input.nextLine();
				switch (yn.toLowerCase()) {
					case "y":
					case "yes":
						fed = true;
						break;
					case "n":
					case "no":
						fed = false;
						break;
					default:
						System.out.println("That is not yes or no. Returning to menu.");
						return data;
				}
				a = new Lion(name, age, weight, fed);
				break;
			case "bear":
				System.out.print("Bear hibernating (y/n): ");
				boolean hibernating = false;
				String yorn = input.nextLine();
				switch (yorn.toLowerCase()) {
					case "y":
					case "yes":
						hibernating = true;
						break;
					case "n":
					case "no":
						hibernating = false;
						break;
					default:
						System.out.println("That is not yes or no. Returning to menu.");
						return data;
				}
				a = new Bear(name, age, weight, hibernating);
				break;
			case "elephant":
				a = new Elephant(name, age, weight);
				break;
			default:
				System.out.println("That is not a valid animal type. Returning to menu.");
				return data;
		}
		
		data = resize(data, data.length+1);
		data[data.length - 1] = a;
		
		return data;
	}
	
	public static void listAnimals(Animal[] data) {
		for (int i=0; i<data.length; i++) {
			System.out.println(data[i]);
		}
	}
	
	public static Animal[] removeAnimal(Animal[] data, Scanner input) {
		Animal[] temp = new Animal[10];
		int count = 0;
		for (int i=0; i<data.length; i++) {
			boolean sell = yesNoPrompt("Sell " + data[i].getName(), input);
			if (!sell) {
				if (temp.length == count) {
					temp = resize(temp, temp.length * 2);
				}
				temp[count] = data[i];
				count++;
			}
		}
		
		temp = resize(temp, count);
		
		return temp;
	}
	
	public static boolean yesNoPrompt(String prompt, Scanner input) {
		System.out.print(prompt + " (y/n): ");
		boolean result = false;
		String yn = input.nextLine();
		switch (yn.toLowerCase()) {
			case "y":
			case "yes":
				result = true;
				break;
			case "n":
			case "no":
				result = false;
				break;
			default:
				System.out.println("That is not yes or no. Using no.");
		}
		
		return result;
	}
	
	public static void saveFile(String filename, Animal[] data) {
		try {
			FileWriter writer = new FileWriter(filename);
			
			for (int i=0; i<data.length; i++) {
				writer.write(data[i].toCSV() + "\n");
				writer.flush();
			}
			
			writer.close();
		} catch (Exception e) {
			System.out.println("Error saving to file.");
		}
	}

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Load file: ");
		String filename = input.nextLine();
		// Load file (if exists)
		Animal[] data = readFile(filename);
		// Menu
		data = menu(input, data);
		// Save file
		saveFile(filename, data);
		input.close();
		System.out.println("Goodbye!");
	}

}
