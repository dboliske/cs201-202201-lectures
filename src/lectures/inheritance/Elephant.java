package lectures.inheritance;

public class Elephant extends Animal {

	public Elephant() {
		super();
	}
	
	public Elephant(String name, int age, double weight) {
		super(name, age, weight);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof Elephant)) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String toString() {
		return super.toString() + " " + super.getName() + " is a elephant.";
	}
	
	@Override
	public String toCSV() {
		return "Elephant," + super.csvData();
	}
	
}
