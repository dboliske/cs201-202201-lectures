package lectures.inheritance;

public class Zoo {

	public static void main(String[] args) {
		Animal a = new Animal();
		Cat c = new Cat();
		Dog d = new Dog();
		
		System.out.println(a.toString());
		System.out.println(c.toString());
		System.out.println(d.toString());
		
		System.out.println(a.equals(c));
		System.out.println(a.equals(d));
		System.out.println(c.equals(a));
		System.out.println(d.equals(a));
		System.out.println(c.equals(d));
		System.out.println(d.equals(c));
		
		Animal[] pets = new Animal[3];
		pets[0] = a;
		pets[1] = c;
		pets[2] = d;
		
		for (int i=0; i<pets.length; i++) {
			System.out.println(pets[i].toString());
		}
	}

}
