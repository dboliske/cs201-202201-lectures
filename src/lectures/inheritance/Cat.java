package lectures.inheritance;

public class Cat extends Animal {

	private String color;
	
	public Cat() {
		super();
		this.color = "black";
	}
	
	public Cat(String name, int age, double weight, String color) {
		super(name, age, weight);
		this.color = color;
	}
	
	public void setColor(String color) {
		this.color = color;
	}
	
	public String getColor() {
		return color;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof Cat)) {
			return false;
		}
		
		Cat c = (Cat)obj;
		if (!this.color.equals(c.getColor())) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String toString() {
		return super.toString() + " " + super.getName() + " is a " + color + " cat.";
	}
	
}
