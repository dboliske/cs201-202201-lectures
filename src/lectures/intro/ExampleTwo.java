package lectures.intro;

public class ExampleTwo {

	public static void main(String[] args) {
		String hello = "Hello";
		
		System.out.println(hello.length()); // number of characters in the string, 5
		System.out.println(hello.charAt(0)); // first character
		System.out.println(hello.charAt(hello.length() - 1)); // last character
		System.out.println(hello.substring(2));
		System.out.println(hello.substring(2, 3));
		System.out.println(hello.replace('e', '3'));
		System.out.println(hello.replaceAll("ll", "1"));
		
	}

}
