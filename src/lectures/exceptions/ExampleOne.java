package lectures.exceptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class ExampleOne {

	public static void main(String[] args) {
		System.out.println("Started");
		try {
			File f = new File("src/lectures/exceptions/unknown.txt");
//			File f = new File("src/lectures/exceptions/Oneline.txt");
			Scanner input = new Scanner(f);
			System.out.println("Starting file content...");
			System.out.println(input.nextLine());
			System.out.println(input.nextLine());
			System.out.println(input.nextLine());
			System.out.println(input.nextLine());
			System.out.println("Closing file");
			input.close();
		} catch (FileNotFoundException e) {
			System.out.println("ERROR!!! File not found!!!");
			System.out.println(e.getMessage());
			System.out.println(e.toString());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("ERROR!!! Unable to read file!!!");
		} catch (NoSuchElementException e) {
			System.out.println("ERROR!!! Unable to read next line!!!");
		} catch (Exception e) {
			System.out.println("ERROR!!!");
		}
		System.out.println("Goodbye");
	}

}
