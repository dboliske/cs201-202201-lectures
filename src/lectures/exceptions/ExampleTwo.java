package lectures.exceptions;

import java.util.Scanner;

public class ExampleTwo {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int n = 0;
		String line = "";
		boolean done = false;
		do {
			try {
				System.out.print("Enter a number: ");
				line = input.nextLine();
				n = Integer.parseInt(line);
				done = true;
			} catch (Exception e) {
				System.out.println("Invalid Input. '" + line + "' is not a valid number.");
			}
		} while (!done);
		
		input.close();
		
		System.out.println(n);
	}

}
