package lectures.exceptions;

import java.io.FileWriter;
import java.io.IOException;

public class ExampleThree {

	public static void main(String[] args) {
		int[] a = {1, 2, 3, 4, 5, 6};
		
		try {
			FileWriter f = new FileWriter("src/lectures/exceptions/output.txt");
			
			for (int i=0; i<a.length; i++) {
				f.write(a[i] + "\n");
			}
			f.flush();
			f.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
		System.out.println("Finished Writing");
		
	}

}
