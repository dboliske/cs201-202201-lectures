package lectures.searching;

import java.util.Scanner;

public class ListClient {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		StringList list = new StringList();
		String[] data = new String[5];
		for (int i=0; i<data.length; i++) {
			System.out.print("Enter in a word: ");
			data[i] = input.nextLine();
			list.add(data[i]);
		}
		
		input.close();
		
		for (int i=0; i<data.length; i++) {
			System.out.println(i + " - " + data[i] + " @ " + list.indexOf(data[i]));
		}
		
		System.out.println("unknown @ " + list.indexOf("unknown"));
	}

}
