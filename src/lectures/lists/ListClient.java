package lectures.lists;

public class ListClient {

	public static void main(String[] args) {
		StringList list = new StringList(20);
		for (int i=0; i<20; i++) {
			list.add(i + "");
		}
		
		System.out.println(list.toString());
		
		for (int i=8; i<=12; i++) {
			list.remove(i+"");
		}
		StringList list2 = new StringList(list.toArray());
		System.out.println(list.toString());
		
		System.out.println("Index of 16: " + list.indexOf("16"));
		System.out.println("Value at index: " + list.get(list.indexOf("16")));
	
		System.out.println("Contains 5: " + list.contains("5"));
		System.out.println("Contains 100: " + list.contains("100"));
		
		for (int i=4; i<10; i++) {
			list.set((char)(i + 'A') + "", i);
		}
		StringList list3 = new StringList(list.toArray());
		System.out.println(list.toString());
	
		System.out.println(list.equals(list2)); // False
		System.out.println(list.equals(list3)); // True
		
		list.clear();
		System.out.println(list.toString());
	}

}
