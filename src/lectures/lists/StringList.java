package lectures.lists;

public class StringList {
	
	private int size;
	private String[] data;
	
	public StringList() {
		size = 0;
		data = new String[10];
	}
	
	public StringList(int capacity) {
		size = 0;
		data = new String[capacity];
	}
	
	public StringList(String[] init) {
		size = init.length;
		data = new String[init.length];
		for (int i=0; i<init.length; i++) {
			data[i] = init[i];
		}
	}
	
	public int size() {
		return size;
	}
	
	public void add(String s) {
		if (size == data.length) {
			// resize
			String[] temp = new String[data.length * 2];
			for (int i=0; i<data.length; i++) {
				temp[i] = data[i];
			}
			data = temp;
			temp = null;
		}
		
		data[size] = s;
		size++;
	}
	
	public void set(String s, int index) {
		if (index >= 0 && index < size) {
			data[index] = s;
		}
	}
	
	public void remove(String s) {
		int index = indexOf(s);
		
		if (index != -1) { // item has been found
			for (int i=(index+1); i<size; i++) {
				data[i-1] = data[i];
			}
			size--;
		}
	}
	
	public void clear() {
		size = 0;
	}
	
	public String get(int index) {
		if (index < 0 || index >= size) {
			return null;
		}
		
		return data[index];
	}
	
	public int indexOf(String s) {
		for (int i=0; i<size; i++) {
			if (data[i].equals(s)) {
				return i;
			}
		}
		
		return -1;
	}
	
	public boolean contains(String s) {
		return indexOf(s) != -1;
	}
	
	public String[] toArray() {
		String[] result = new String[size];
		for (int i=0; i<size; i++) {
			result[i] = data[i];
		}
		
		return result;
	}
	
	public String toString() {
		String result = "[";
		
		if (size > 0) {
			result += data[0];
			for (int i=1; i<size; i++) {
				result += ", " + data[i];
			}
		}
		
		result += "]";
		
		return result;
	}
	
	public boolean equals(Object obj) {
		if (!(obj instanceof StringList)) {
			return false;
		}
		StringList l = (StringList)obj;
		if (size != l.size()) {
			return false;
		}
		
		for (int i=0; i<size; i++) {
			if (!data[i].equals(l.get(i))) {
				return false;
			}
		}
		
		return true;
	}

}
