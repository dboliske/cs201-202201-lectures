package lectures.lists;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

import lectures.inheritance.Animal;
import lectures.inheritance.Bear;
import lectures.inheritance.Elephant;
import lectures.inheritance.Lion;

public class ZooClient {
	
	public static ArrayList<Animal> readFile(String filename) {
		ArrayList<Animal> animals = new ArrayList<Animal>();
		
		try {
			File f = new File(filename);
			Scanner input = new Scanner(f);
			
			// end-of-file loop
			while (input.hasNextLine()) {
				try {
					String line = input.nextLine();
					String[] values = line.split(",");
					Animal a = null;
					switch (values[0].toLowerCase()) {
						case "elephant":
							a = new Elephant(
								values[1],
								Integer.parseInt(values[2]),
								Double.parseDouble(values[3])
							);
							break;
						case "lion":
							a = new Lion(
									values[1],
									Integer.parseInt(values[2]),
									Double.parseDouble(values[3]),
									Boolean.parseBoolean(values[4])
								);
							break;
						case "bear":
							a = new Bear(
									values[1],
									Integer.parseInt(values[2]),
									Double.parseDouble(values[3]),
									Boolean.parseBoolean(values[4])
								);
							break;
					}
					animals.add(a);
				} catch (Exception e) {
					
				}
			}
			
			input.close();
		} catch (FileNotFoundException fnf) {
			// Do nothing.
		} catch(Exception e) {
			System.out.println("Error occurred reading in file.");
		}
		
		return animals;
	}
	
	public static ArrayList<Animal> menu(Scanner input, ArrayList<Animal> data) {
		boolean done = false;
		
		String[] options = {"Purchase Animal", "Sell Animal", "List Animals", "Exit"};
		
		do {
			for (int i=0; i<options.length; i++) {
				System.out.println((i+1) + ". " + options[i]);
			}
			System.out.print("Choice: ");
			String choice = input.nextLine();
			
			switch (choice) {
				case "1": // Create new animal
					data = createAnimal(data, input);
					break;
				case "2": // Remove existing animal
					data = removeAnimal(data, input);
					break;
				case "3": // List animals
					listAnimals(data);
					break;
				case "4": // Exit
					done = true;
					break;
				default:
					System.out.println("I'm sorry, but I didn't understand that.");
			}
		} while (!done);
		
		return data;
	}
	
	public static ArrayList<Animal> createAnimal(ArrayList<Animal> data, Scanner input) {
		
		Animal a;
		System.out.print("Animal Name: ");
		String name = input.nextLine();
		System.out.print("Animal Age: ");
		int age = 0;
		try {
			age = Integer.parseInt(input.nextLine());
		} catch (Exception e) {
			System.out.println("That is not a valid age. Returning to menu.");
			return data;
		}
		System.out.print("Animal Weight: ");
		double weight = 0;
		try {
			weight = Double.parseDouble(input.nextLine());
		} catch (Exception e) {
			System.out.println("That is not a valid weight. Returning to menu.");
			return data;
		}
		
		System.out.print("Type of Animal (Lion, Bear, or Elephant): ");
		String type = input.nextLine();
		switch (type.toLowerCase()) {
			case "lion":
				System.out.print("Lion fed (y/n): ");
				boolean fed = false;
				String yn = input.nextLine();
				switch (yn.toLowerCase()) {
					case "y":
					case "yes":
						fed = true;
						break;
					case "n":
					case "no":
						fed = false;
						break;
					default:
						System.out.println("That is not yes or no. Returning to menu.");
						return data;
				}
				a = new Lion(name, age, weight, fed);
				break;
			case "bear":
				System.out.print("Bear hibernating (y/n): ");
				boolean hibernating = false;
				String yorn = input.nextLine();
				switch (yorn.toLowerCase()) {
					case "y":
					case "yes":
						hibernating = true;
						break;
					case "n":
					case "no":
						hibernating = false;
						break;
					default:
						System.out.println("That is not yes or no. Returning to menu.");
						return data;
				}
				a = new Bear(name, age, weight, hibernating);
				break;
			case "elephant":
				a = new Elephant(name, age, weight);
				break;
			default:
				System.out.println("That is not a valid animal type. Returning to menu.");
				return data;
		}
		
		data.add(a);
		
		return data;
	}
	
	public static void listAnimals(ArrayList<Animal> data) {
		for (Animal a : data) {
			System.out.println(a);
		}
	}
	
	public static ArrayList<Animal> removeAnimal(ArrayList<Animal> data, Scanner input) {
		ArrayList<Animal> toRemove = new ArrayList<Animal>();
		for (Animal a : data) {
			boolean sell = yesNoPrompt("Sell " + a.getName(), input);
			if (sell) {
				toRemove.add(a);
			}
		}
		
		for (Animal a : toRemove) {
			data.remove(a);
		}
		
		return data;
	}
	
	public static boolean yesNoPrompt(String prompt, Scanner input) {
		System.out.print(prompt + " (y/n): ");
		boolean result = false;
		String yn = input.nextLine();
		switch (yn.toLowerCase()) {
			case "y":
			case "yes":
				result = true;
				break;
			case "n":
			case "no":
				result = false;
				break;
			default:
				System.out.println("That is not yes or no. Using no.");
		}
		
		return result;
	}
	
	public static void saveFile(String filename, ArrayList<Animal> data) {
		try {
			FileWriter writer = new FileWriter(filename);
			
			for (Animal a : data) {
				writer.write(a.toCSV() + "\n");
				writer.flush();
			}
			
			writer.close();
		} catch (Exception e) {
			System.out.println("Error saving to file.");
		}
	}

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Load file: ");
		String filename = input.nextLine();
		// Load file (if exists)
		ArrayList<Animal> data = readFile(filename);
		// Menu
		data = menu(input, data);
		// Save file
		saveFile(filename, data);
		input.close();
		System.out.println("Goodbye!");
	}

}
