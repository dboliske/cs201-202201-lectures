package lectures.recursion;

public class ExcerciseOne {
	
	public static void printRev(int[] data) {
		printRev(data, 0, data.length - 1);
	}
	
	public static void printRev(int[] data, int first, int last) {
		if (first > last) {
			return;
		}
		
		System.out.print(data[last] + " ");
		printRev(data, first, last - 1);
	}

	public static void main(String[] args) {
		int[] data = {74, 36, 87, 95, 100, 42};
		printRev(data);
	}

}
