package lectures.interfaces;

public class Example {

	public static void main(String[] args) {
		Character player = new Character();
		Creature rat = new Creature();
		
		System.out.println(player);
		System.out.println(rat);
		
		System.out.println(player.compareTo(rat));
		System.out.println(rat.compareTo(player));
		
		Comparable[] c = {player, rat};
		System.out.println(c[0].compareTo(c[1]));
	}

}
