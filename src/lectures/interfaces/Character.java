package lectures.interfaces;

public class Character implements Comparable {

	private String name;
	private int health;
	
	public Character() {
		name = "Steve";
		health = 10;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		if (health >= 0) {
			this.health = health;
		}
	}
	
	@Override
	public String toString() {
		return name;
	}

	@Override
	public int compareTo(Object obj) {		
		if (obj instanceof Character) {
			Character c = (Character)obj;
			return name.compareToIgnoreCase(c.getName());
		} else if (obj instanceof Creature) {
			Creature c = (Creature)obj;
			return name.compareToIgnoreCase(c.getName());
		}
		
		return 0;
	}

}
