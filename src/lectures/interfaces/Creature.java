package lectures.interfaces;

public class Creature implements Comparable {
	
	private String name;
	private int health;
	private boolean evil;
	
	public Creature() {
		name = "rat";
		health = 1;
		evil = false;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		if (health >= 0) {
			this.health = health;
		}
	}

	public boolean isEvil() {
		return evil;
	}

	public void setEvil(boolean evil) {
		this.evil = evil;
	}
	
	@Override
	public String toString() {
		return name;
	}

	@Override
	public int compareTo(Object obj) {		
		if (obj instanceof Character) {
			Character c = (Character)obj;
			return name.compareToIgnoreCase(c.getName());
		} else if (obj instanceof Creature) {
			Creature c = (Creature)obj;
			return name.compareToIgnoreCase(c.getName());
		}
		
		return 0;
	}

}
