package lectures.interfaces;

public interface Comparable {
	public abstract int compareTo(Object obj);
}
