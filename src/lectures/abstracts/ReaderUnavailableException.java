package lectures.abstracts;

public class ReaderUnavailableException extends Exception {
	
	private String message;
	
	public ReaderUnavailableException() {
		message = "The specified reader is unavailable";
	}
	
	public ReaderUnavailableException(String msg) {
		message = msg;
	}

	@Override
	public String toString() {
		return message;
	}
	
	@Override
	public String getMessage() {
		return message;
	}
	
}
