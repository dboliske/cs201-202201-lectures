package lectures.abstracts;

import java.util.Scanner;

public class UserReader extends Reader {
	
	public UserReader() {
		super();
		setInput(new Scanner(System.in));
	}

	@Override
	public String nextLine(String prompt) throws ReaderUnavailableException {
		if (this.input == null) {
			throw new ReaderUnavailableException();
		}
		
		System.out.print(prompt);
		return this.input.nextLine();
	}

	@Override
	public int nextInt(String prompt) throws ReaderUnavailableException {
		if (this.input == null) {
			throw new ReaderUnavailableException();
		}
		
		while (true) {
			System.out.print(prompt);
			try {
				String in = input.nextLine();
				int value = Integer.parseInt(in);
				return value;
			} catch (Exception e) {
				System.out.println("Please enter an integer.");
			}
		}
	}

	@Override
	public double nextDouble(String prompt) throws ReaderUnavailableException {
		if (this.input == null) {
			throw new ReaderUnavailableException();
		}
		
		while (true) {
			System.out.print(prompt);
			try {
				String in = input.nextLine();
				double value = Double.parseDouble(in);
				return value;
			} catch (Exception e) {
				System.out.println("Please enter a number.");
			}
		}
	}

}
