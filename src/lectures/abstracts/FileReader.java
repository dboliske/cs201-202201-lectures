package lectures.abstracts;

import java.io.File;
import java.util.Scanner;

public class FileReader extends Reader {
	
	private String fileName;
	
	public FileReader(String fn) {
		super();
		this.fileName = fn;
		setInput(fn);
	}
	
	public void setInput(String fn) {
		try {
			File f = new File(fn);
			input = new Scanner(f);
		} catch (Exception e) {
			System.out.println("File does not exist...");
		}
	}

	@Override
	public String nextLine(String prompt) throws ReaderUnavailableException {
		if (input == null) {
			throw new ReaderUnavailableException();
		} else if (!input.hasNextLine()) {
			throw new ReaderUnavailableException("The specified reader contains no more input.");
		}
		System.out.print(prompt);
		String in = input.nextLine();
		System.out.println(in);
		return in;
	}

	@Override
	public int nextInt(String prompt) throws ReaderUnavailableException {
		if (input == null) {
			throw new ReaderUnavailableException();
		} else if (!input.hasNextLine()) {
			throw new ReaderUnavailableException("The specified reader contains no more input.");
		}
		System.out.print(prompt);
		String in = input.nextLine();
		System.out.println(in);
		return Integer.parseInt(in);
	}

	@Override
	public double nextDouble(String prompt) throws ReaderUnavailableException {
		if (input == null) {
			throw new ReaderUnavailableException();
		} else if (!input.hasNextLine()) {
			throw new ReaderUnavailableException("The specified reader contains no more input.");
		}
		
		System.out.print(prompt);
		String in = input.nextLine();
		System.out.println(in);
		return Double.parseDouble(in);
	}

}
