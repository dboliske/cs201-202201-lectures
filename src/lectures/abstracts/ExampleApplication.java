package lectures.abstracts;

public class ExampleApplication {
	
	public static void addition(Reader input) throws ReaderUnavailableException {
		double x = input.nextDouble("X=");
		double y = input.nextDouble("Y=");
		System.out.println("X + Y = " + (x+y));
	}
	
	public static void multiplication(Reader input) throws ReaderUnavailableException {
		double x = input.nextDouble("X=");
		double y = input.nextDouble("Y=");
		System.out.println("X * Y = " + (x*y));
	}

	public static void main(String[] args) {
		// Create 'Reader'
//		Reader input = new UserReader();
//		Reader input = new FileReader("src/lectures/abstracts/unknown.txt");
		Reader input = new FileReader("src/lectures/abstracts/input.txt");
//		Reader input = new FileReader("src/lectures/abstracts/unending-input.txt");
		// Menu
		boolean done = false;
		
		do {
			try {
				String choice = input.nextLine("1. Addition\n2. Multiplication\n3. Exit\nChoice: ");
				switch (choice) {
					case "1":
						addition(input);
						break;
					case "2":
						multiplication(input);
						break;
					case "3":
						done = true;
						break;
					default:
						System.out.println("I didn't understand that...");
				}
			} catch (ReaderUnavailableException rue) {
				System.out.println(rue.getMessage());
				done = true;
			}
		} while (!done);
		
		input.close();
	}

}
