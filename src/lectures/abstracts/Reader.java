package lectures.abstracts;

import java.util.Scanner;

public abstract class Reader {

	protected Scanner input;
	
	public Reader() {
		input = null;
	}

	public Scanner getInput() {
		return input;
	}

	public void setInput(Scanner input) {
		this.input = input;
	}
	
	public void close() {
		if (input != null) {
			input.close();
			input = null;
		}
	}
	
	public abstract String nextLine(String prompt) throws ReaderUnavailableException;
	public abstract int nextInt(String prompt) throws ReaderUnavailableException;
	public abstract double nextDouble(String prompt) throws ReaderUnavailableException;
	
}
