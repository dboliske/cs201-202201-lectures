package lectures.generics;

public class PairApp {

	public static void main(String[] args) {
		OurList<Pair<Integer, String>> list = new OurList<Pair<Integer, String>>();
		
		for (int i=0; i<5; i++) {
			Pair<Integer, String> p = new Pair<Integer, String>((i+1), (char)('a' + i) + "");
			list.add(p);
		}
		
		System.out.println(list);
	}

}
