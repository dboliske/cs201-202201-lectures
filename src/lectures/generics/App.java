package lectures.generics;

public class App {

	public static void main(String[] args) {
		OurList<Integer> ints = new OurList<Integer>();
		for (int i=1; i<=5; i++) {
			ints.add(i);
		}
		
		System.out.println(ints);
		
		OurList<String> strings = new OurList<String>();
		for (char c='a'; c<='e'; c++) {
			strings.add(c + "");
		}
		
		System.out.println(strings);
	}

}
