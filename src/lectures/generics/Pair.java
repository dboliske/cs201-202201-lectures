package lectures.generics;

public class Pair<K, V> {
	
	private K key;
	private V value;
	
	public Pair(K k) {
		key = k;
		value = null;
	}
	
	public Pair(K k, V v) {
		key = k;
		value = v;
	}
	
	public K key() {
		return key;
	}
	
	public V value() {
		return value;
	}
	
	public V get() {
		return value;
	}
	
	public void set(V v) {
		value = v;
	}
	
	public String toString() {
		return key.toString() + " - " + value.toString();
	}
	
	public boolean equals(Object obj) {
		if (!(obj instanceof Pair)) {
			return false;
		}
		
		Pair<K, V> p = (Pair<K, V>)obj;
		
		if (!(key.equals(p.key()))) {
			return false;
		} else if (!(value.equals(p.get()))) {
			return false;
		}
		
		
		return true;
	}

}
