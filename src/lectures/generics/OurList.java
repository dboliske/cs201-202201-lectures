package lectures.generics;

public class OurList<E> {
	
	private int size;
	private Object[] data;
	
	public OurList() {
		size = 0;
		data = new Object[10];
	}
	
	public OurList(int capacity) {
		size = 0;
		data = new Object[capacity];
	}
	
	public OurList(E[] init) {
		size = init.length;
		data = new Object[init.length];
		for (int i=0; i<init.length; i++) {
			data[i] = init[i];
		}
	}
	
	public int size() {
		return size;
	}
	
	public void add(E s) {
		if (size == data.length) {
			// resize
			Object[] temp = new Object[data.length * 2];
			for (int i=0; i<data.length; i++) {
				temp[i] = data[i];
			}
			data = temp;
			temp = null;
		}
		
		data[size] = s;
		size++;
	}
	
	public void set(E s, int index) {
		if (index >= 0 && index < size) {
			data[index] = s;
		}
	}
	
	public void remove(E s) {
		int index = indexOf(s);
		
		if (index != -1) { // item has been found
			for (int i=(index+1); i<size; i++) {
				data[i-1] = data[i];
			}
			size--;
		}
	}
	
	public void clear() {
		size = 0;
	}
	
	public E get(int index) {
		if (index < 0 || index >= size) {
			return null;
		}
		
		return (E)data[index];
	}
	
	public int indexOf(E s) {
		for (int i=0; i<size; i++) {
			if (data[i].equals(s)) {
				return i;
			}
		}
		
		return -1;
	}
	
	public boolean contains(E s) {
		return indexOf(s) != -1;
	}
	
	public E[] toArray() {
		Object[] result = new Object[size];
		for (int i=0; i<size; i++) {
			result[i] = data[i];
		}
		
		return (E[])result;
	}
	
	public String toString() {
		String result = "[";
		
		if (size > 0) {
			result += data[0];
			for (int i=1; i<size; i++) {
				result += ", " + data[i];
			}
		}
		
		result += "]";
		
		return result;
	}
	
	public boolean equals(Object obj) {
		if (!(obj instanceof OurList<?>)) {
			return false;
		}
		OurList<E> l = (OurList<E>)obj;
		if (size != l.size()) {
			return false;
		}
		
		for (int i=0; i<size; i++) {
			if (!data[i].equals(l.get(i))) {
				return false;
			}
		}
		
		return true;
	}

}
