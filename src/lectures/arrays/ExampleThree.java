package lectures.arrays;

import java.util.Scanner;

public class ExampleThree {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); //create scanner for user input
		
		double[] values = new double[5];
		for(int i=0; i<values.length; i++) {
			System.out.print("(" + (i+1) + ") number: ");
			values[i] = Double.parseDouble(input.nextLine());
		}
		
		boolean done = false;
		
		do {
			System.out.println("1. Mean");
			System.out.println("2. Min");
			System.out.println("3. Max");
			System.out.println("4. Total");
			System.out.println("5. Exit");
			
			System.out.print("Choice: ");
			String choice = input.nextLine();
			double total = 0.0;
			switch(choice) {
				case "1": // mean
					total = 0;
					for (int i=0; i<values.length; i++) {
						total = total + values[i];
					}
					
					System.out.println("Mean: " + (total / values.length));
					break;
				case "2": // min
					double min = values[0];
					for (int i=1; i<values.length; i++) {
						if (values[i] < min) {
							min = values[i];
						}
					}
					System.out.println("Min: " + min);
					break;
				case "3": // max
					double max = values[0];
					for (int i=1; i<values.length; i++) {
						if (values[i] > max) {
							max = values[i];
						}
					}
					System.out.println("Max: " + max);
					break;
				case "4": // total
					total = 0;
					for (int i=0; i<values.length; i++) {
						total = total + values[i];
					}
					
					System.out.println("Total: " + total);
					break;
				case "5": // exit
					done = true;
					break;
				default:
					System.out.println("What?");
			}
		} while (!done);
		
		input.close();
		
		System.out.println("Goodbye!");
		
	}

}
