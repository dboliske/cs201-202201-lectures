package lectures.arrays;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class ExampleFive {

	public static void main(String[] args) throws IOException {
		// create our file object
		File f = new File("src/lectures/arrays/weather.csv");
		Scanner input = new Scanner(f);
		
		// create arrays
		final int SIZE = 5;
		String[] dates = new String[SIZE];
		int[] minTemps = new int[SIZE];
		int[] maxTemps = new int[SIZE];
		int count = 0;
		// end-of-file loop
		while (input.hasNextLine()) {
			String line = input.nextLine();
			String[] values = line.split(","); // {"2022-01-01", "20", "40"}
			dates[count] = values[0];
			minTemps[count] = Integer.parseInt(values[1]);
			maxTemps[count] = Integer.parseInt(values[2]);
			count++;
		}
		
		input.close();
		
		// print out
		for (int i=0; i<SIZE; i++) {
			System.out.println(dates[i] + " - min: " + minTemps[i] + ";max: " + maxTemps[i]);
		}
	}

}
