package lectures.arrays;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class ExampleFour {

	public static void main(String[] args) throws IOException {
		File f = new File("src/lectures/arrays/weather.csv");
		Scanner input = new Scanner(f);
		
		int[] temps = new int[5];
		int count = 0;
		while (input.hasNextLine()) {
			String line = input.nextLine();
			String[] values = line.split(","); // {"2022-01-01", "20", "40"}
			temps[count] = Integer.parseInt(values[1]);
			count++;
		}
		
		input.close();
		
		for (int i=0; i<temps.length; i++) {
			System.out.println(temps[i]);
		}
	}

}
