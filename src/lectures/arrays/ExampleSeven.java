package lectures.arrays;

public class ExampleSeven {

	public static void main(String[] args) {
		int[] a = {1, 2, 3};
		// int[] b = {3, 2};
		// int[] b = {3, 2, 1};
		// int[] b = {1, 2};
		int[] b = {1, 2, 3};
		
		// 1. Confirm the length
		if (a.length == b.length) {
			// 2. Confirm that they contain the same values
			boolean equal = true;
			for (int i=0; i<a.length && equal; i++) {
				if (a[i] != b[i]) {
					equal = false;
				}
			}
			
			if (equal) {
				System.out.println("Equal!");
			} else {
				System.out.println("Not Equal. Not the same values.");
			}
		} else {
			System.out.println("Not Equal. Not the same length.");
		}
	}

}
