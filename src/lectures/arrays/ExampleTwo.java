package lectures.arrays;

import java.util.Scanner;

public class ExampleTwo {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); //create scanner for user input
		
		double[] values = new double[5];
		for(int i=0; i<values.length; i++) {
			System.out.print("(" + (i+1) + ") number: ");
			values[i] = Double.parseDouble(input.nextLine());
		}
		
		input.close();
		
		double total = 0;
		for (int i=0; i<values.length; i++) {
			total = total + values[i];
		}
		
		System.out.println("Average: " + (total / values.length));
	}

}
