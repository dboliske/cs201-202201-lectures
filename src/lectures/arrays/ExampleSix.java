package lectures.arrays;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class ExampleSix {

	public static void main(String[] args) throws IOException {
		// create our file object
		File f = new File("src/lectures/arrays/weather.csv");
		Scanner input = new Scanner(f);
		
		// create array
		int[] temps = new int[2];
		int count = 0;
		while (input.hasNextLine()) {
			String[] values = input.nextLine().split(",");
			if (count == temps.length) {
				// resize the array
				int[] bigger = new int[2 * temps.length];
				for (int i=0; i<temps.length; i++) {
					bigger[i] = temps[i];
				}
				temps = bigger;
				bigger = null;
			}
			temps[count] = Integer.parseInt(values[1]);
			count++;
		}
		
		input.close();
		
		// trim array down
		int[] smaller = new int[count];
		for (int i=0; i<smaller.length; i++) {
			smaller[i] = temps[i];
		}
		temps = smaller;
		smaller = null;
		
		// print out array
		for (int i=0; i<temps.length; i++) {
			System.out.println(temps[i]);
		}
	}

}
