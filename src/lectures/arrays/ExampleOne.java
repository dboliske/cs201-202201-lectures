package lectures.arrays;

public class ExampleOne {

	public static void main(String[] args) {
		double[] temps = new double[5];
		int m = 4;
		temps[2] = 98.6;
		temps[3] = 101.2;
		temps[0] = 90.0;
		temps[m] = temps[3] / 2.0;
		temps[1] = temps[3] - 1.2;
		
		for (int i = 0; i < temps.length; i++) {
			System.out.println(temps[i]);
		}
	}

}
