package lectures.selection;

public class ExampleThree {

	public static void main(String[] args) {
		// Finding the minimum
		
		int x = 2;
		int y = 3;
		
		// finding min with if-else
//		int min = x;
//		if (y < x) {
//			min = y;
//		}
		
		// finding min with conditional operator
		int min = (x < y) ? x : y;
		
		System.out.println("The minimum is " + min);
	}

}
