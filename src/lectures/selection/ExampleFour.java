package lectures.selection;

import java.util.Scanner;

public class ExampleFour {

	public static void main(String[] args) {
		// create a variable for reading in user input
		Scanner input = new Scanner(System.in);
		
		// prompt the user to type something
		System.out.print("Prompt: ");
		
		// read in the next line of text typed by the user
		String value = input.nextLine();
		
		// print out what the user entered
		System.out.println("Echo: " + value);
	}

}
