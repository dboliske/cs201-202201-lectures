package lectures.selection;

import java.util.Scanner;

public class ExampleSeven {

	public static void main(String[] args) {
		// create a scanner
		Scanner input = new Scanner(System.in);
		
		// prompt for x
		System.out.print("X: ");
		double x = Double.parseDouble(input.nextLine());

		// prompt for y
		System.out.print("Y: ");
		double y = Double.parseDouble(input.nextLine());
		
		// prompt for operation
		System.out.print("Operation: ");
		char op = input.nextLine().charAt(0);
		
		input.close();
		
		if (op == '+') {
			System.out.println("x+y="+(x+y));
		} else if (op == '-') {
			System.out.println("x-y="+(x-y));
		} else if (op == '*') {
			System.out.println("x*y="+(x*y));
		} else if (op == '/') {
			System.out.println("x/y="+(x/y));
		} else {
			System.out.println("unknown operation");
		}
	}

}
