package lectures.selection;

import java.util.Scanner;

public class ExampleFourteen {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create a scanner
		
		System.out.print("Enter a word: ");
		String word = input.nextLine(); // get input
		
		input.close(); // close our scanner
		
		if (word.compareTo("monkey") < 0) {
			System.out.println("before");
		} else if (word.compareTo("monkey") > 0) {
			System.out.println("after");
		} else {
			System.out.println("is monkey");
		}
	}

}
