package lectures.selection;

public class ExampleTwo {

	public static void main(String[] args) {
		char c = 'd';
		
		if (c == 'a') {
			System.out.println("apple");
		} else if (c == 'b') {
			System.out.println("bat");
		} else if (c == 'c') {
			System.out.println("cat");
		} else {
			System.out.println("not a, b, or c");
		}
	}

}
