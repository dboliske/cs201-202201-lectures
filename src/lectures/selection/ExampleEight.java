package lectures.selection;

import java.util.Scanner;

public class ExampleEight {

	public static void main(String[] args) {
		// create a scanner
		Scanner input = new Scanner(System.in);
		
		// prompt the user
		System.out.print("X: ");
		char c = input.nextLine().charAt(0);
		
		if (c >= '0' && c <= '9') {
			System.out.println("Number!");
		} else {
			System.out.println("Not Number!");
		}
		
		input.close();
	}

}
