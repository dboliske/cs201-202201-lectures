package lectures.selection;

public class ExampleOne {

	public static void main(String[] args) {
		int a = 11;
		
		if (a % 2 == 0) { // check to see if a is even
			System.out.println("A is even");
		} else {
			System.out.println("A is odd");
		}
	}

}
