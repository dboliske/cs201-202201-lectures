package lectures.selection;

import java.util.Scanner;

public class ExampleTwelve {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create scanner for user input
		
		System.out.print("Enter a, b, or c: ");
		char c = input.nextLine().charAt(0);
		
		if (c == 'a' || c == 'A') {
			System.out.println("Apple");
		} else if (c == 'b' || c == 'B') {
			System.out.println("Bat");
		} else if (c == 'c' || c == 'C') {
			System.out.println("Cat");
		} else {
			System.out.println("Not a, b, or c");
		}
		
		input.close();
	}

}
