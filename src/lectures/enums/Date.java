package lectures.enums;

import java.util.regex.Pattern;

public class Date {
	
	private int year;
	private Month month;
	private int day;
	
	private static String datePattern = "([1-9]|[0][1-9]|[12][0-9]|[3][01])(-|/)"
			+ "([1-9]|[0][1-9]|[1][0-2])(-|/)"
			+ "([0-9]+)";
	
	public Date() {
		year = 1970;
		month = Month.JANUARY;
		day = 1;
	}
	
	public Date(int year, int month, int day) {
		this();
		setYear(year);
		setMonth(month);
		setDay(day);
	}
	
	public Date(int year, Month month, int day) {
		this();
		setYear(year);
		this.month = month;
		setDay(day);
	}
	
	public Date(String d) {
		this();
		if (Pattern.matches(datePattern, d)) {
			String[] values = d.split("-|/");
			setYear(Integer.parseInt(values[2]));
			setMonth(Integer.parseInt(values[1]));
			setDay(Integer.parseInt(values[0]));
		}
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public Month getMonth() {
		return month;
	}

	public void setMonth(int month) {
		Month m = Month.getMonth(month);
		if (m != null) {
			this.month = m;
		}
	}
	
	public void setMonth(Month month) {
		this.month = month;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		if (day >= 1 && day <= month.numberOfDays()) {
			this.day = day;
		}
	}
	
	@Override
	public String toString() {
		return year + "-" + month.number() + "-" + day;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null) {
			return false;
		} else if (!(obj instanceof Date)) {
			return false;
		}
		
		Date d = (Date) obj;
		if (year != d.getYear()) {
			return false;
		} else if (month != d.getMonth()) {
			return false;
		} else if (day != d.getDay()) {
			return false;
		}
		
		return true;
	}
	
	public static Date parseDate(String d) {
		if (Pattern.matches(datePattern, d)) {
			return new Date(d);
		}
		
		return null;
	}

}
