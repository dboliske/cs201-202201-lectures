package lectures.enums;

public class DateApplication {

	public static void main(String[] args) {
		Date d1 = new Date();
		System.out.println(d1);
		Date d2 = new Date(2022, Month.APRIL, 4);
		System.out.println(d2);
		Date d3 = new Date(2022, 4, 4);
		System.out.println(d3);
		Date d4 = new Date("4-4-2022");
		System.out.println(d4);
		
		System.out.println(Date.parseDate("18-4-2022"));
		System.out.println(Date.parseDate("42-13-0"));
	}

}
