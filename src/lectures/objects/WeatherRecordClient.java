package lectures.objects;

public class WeatherRecordClient {

	public static void main(String[] args) {
		WeatherRecord wr = new WeatherRecord();
		WeatherRecord today = new WeatherRecord(new DateTime(2022,2,14), 0, 12);
		
		System.out.println(wr);
		System.out.println(today);
		System.out.println(wr.equals(today));
	}

}
