package lectures.objects;

public class DateTimeClient {

	public static void main(String[] args) {
		DateTime d1 = new DateTime();
		
		System.out.println(d1.toString());
		d1.setYear(2022);
		System.out.println(d1);
		d1.setMonth(2);
		System.out.println(d1);
		d1.setMonth(14);
		System.out.println(d1);
		d1.setDay(9);
		System.out.println(d1);
		d1.setDay(42);
		System.out.println(d1);
		d1.setHour(17);
		System.out.println(d1);
		d1.setHour(32);
		System.out.println(d1);
		d1.setMinute(43);
		System.out.println(d1);
		d1.setMinute(90);
		System.out.println(d1);
		d1.setSecond(30);
		System.out.println(d1);
		d1.setSecond(90);
		System.out.println(d1);
		
		DateTime d2 = new DateTime(2022, 2, 9, 17, 43, 30);
		System.out.println(d2);
		System.out.println(d1.equals(d2));
		DateTime d3 = new DateTime(2022, 2, 9, 17, 58, 30);
		System.out.println(d3);
		System.out.println(d1.equals(d3));
		
		System.out.println(d2.before(d3));
		System.out.println(d3.before(d2));
		System.out.println(d2.after(d3));
		System.out.println(d3.after(d2));

	}

}
