package lectures.objects;

public class WeatherRecord {

	private DateTime date;
	private double low;
	private double high;
	
	public WeatherRecord() {
		date = new DateTime();
		low = 0;
		high = 0;
	}
	
	public WeatherRecord(DateTime date, double low, double high) {
		this.date = date;
		this.low = low;
		this.high = high;
	}
	
	public void setDate(DateTime date) {
		this.date = date;
	}
	
	public void setLow(double low) {
		this.low = low;
	}
	
	public void setHigh(double high) {
		this.high = high;
	}
	
	public DateTime getDate() {
		return date;
	}
	
	public double getLow() {
		return low;
	}
	
	public double getHigh() {
		return high;
	}
	
	public boolean equals(WeatherRecord wr) {
		return this.date.equals(wr.getDate()) && this.low == wr.getLow() && this.high == wr.getHigh();
	}
	
	public String toString() {
		return this.date.getYear() + "-" +
			(this.date.getMonth()<10?("0" + this.date.getMonth()): this.date.getMonth()) + "-" +
			(this.date.getDay()<10?("0" + this.date.getDay()): this.date.getDay()) + "," +
			low + "," + high;
	}
	
}
