package lectures.objects;

public class DateTime {
	
	private int year;
	private int month;
	private int day;
	private int hour;
	private int minute;
	private double second;
	
	public DateTime() {
		year = 1970;
		month = 1;
		day = 1;
		hour = 0;
		minute = 0;
		second = 0.0;
	}
	
	public DateTime(int y, int m, int d) {
		year = y;
		month = 1;
		setMonth(m);
		day = 1;
		setDay(d);
		hour = 0;
		minute = 0;
		second = 0.0;
	}
	
	public DateTime(int y, int m, int d, int h, int min, double sec) {
		year = y;
		month = 1;
		setMonth(m);
		day = 1;
		setDay(d);
		hour = 0;
		setHour(h);
		minute = 0;
		setMinute(min);
		second = sec;
	}
	
	public void setYear(int year) {
		this.year = year;
	}
	
	public void setMonth(int month) {
		if (month >= 1 && month <= 12) {
			this.month = month;
		}
	}
	
	public void setDay(int day) {
		if (day >= 1 && day <= 31) {
			this.day = day;
		}
	}
	
	public void setHour(int hour) {
		if (hour >= 0 && hour <= 23) {
			this.hour = hour;
		}
	}
	
	public void setMinute(int minute) {
		if (minute >= 0 && minute <= 59) {
			this.minute = minute;
		}
	}
	
	public void setSecond(double seconds) {
		if (seconds >= 0 && seconds < 60) {
			this.second = seconds;
		}
	}
	
	public int getYear() {
		return year;
	}
	
	public int getMonth() {
		return month;
	}
	
	public int getDay() {
		return day;
	}
	
	public int getHour() {
		return hour;
	}
	
	public int getMinute() {
		return minute;
	}
	
	public double getSecond() {
		return second;
	}
	
	public String toString() {
		return year + "-" +
			(month<10?("0" + month): month) + "-" +
			(day<10?("0" + day): day) + "T" +
			(hour<10?("0" + hour): hour) + ":" +
			(minute<10?("0" + minute): minute) + ":" +
			(second<10?("0" + second): second); 
	}
	
	public boolean equals(DateTime d) {
		if (this.year != d.getYear()) {
			return false;
		} else if (this.month != d.getMonth()) {
			return false;
		} else if (this.day != d.getDay()) {
			return false;
		} else if (this.hour != d.getHour()) {
			return false;
		} else if (this.minute != d.getMinute()) {
			return false;
		} else if (Math.abs(this.second - d.getSecond()) >= 0.1) {
			return false;
		}
		
		return true;
	}
	
	public boolean before(DateTime d) {
		if (this.year > d.getYear()) {
			return false;
		} else if (this.year == d.getYear()) {
			if (this.month > d.getMonth()){
				return false;
			} else if (this.month == d.getMonth()) {
				if (this.day > d.getDay()) {
					return false;
				} else if (this.day == d.getDay()) {
					if (this.hour > d.getHour()) {
						return false;
					} else if (this.hour == d.getHour()) {
						if (this.minute > d.getMinute()) {
							return false;
						} else if (this.minute == d.getMinute()) {
							if (this.second > d.getSecond()) {
								return false;
							}
						}
					}
				}
			}
		}
		
		return true;
	}
	
	public boolean after(DateTime d) {
		return !equals(d) && !before(d);
	}

}