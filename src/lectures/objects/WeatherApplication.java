package lectures.objects;

import java.io.File;
import java.util.Scanner;

public class WeatherApplication {

	public static WeatherRecord[] readData(String filename) {
		WeatherRecord[] data = new WeatherRecord[10];
		int count = 0;
		
		try {
			File f = new File(filename);
			Scanner input = new Scanner(f);
			
			while (input.hasNextLine()) {
				try {
					String line = input.nextLine(); // 2022-01-01,20,40
					String[] values = line.split(","); // {2022-01-01, 20, 40}
					String[] dateValues = values[0].split("-");
					DateTime d = new DateTime(
						Integer.parseInt(dateValues[0]),
						Integer.parseInt(dateValues[1]),
						Integer.parseInt(dateValues[2])
					);
					double low = Double.parseDouble(values[1]);
					double high = Double.parseDouble(values[2]);
					
					WeatherRecord record = new WeatherRecord(d, low, high); // new record
					
					if (count == data.length) { // need to increase array size
						data = resize(data, 2 * data.length);
					}
					
					data[count] = record;
					count++;
				} catch (Exception e) {
					// a line failed to be read in
				}
			}
			
			input.close();
		} catch (Exception e) {
			System.out.println("Something went wrong reading in our data.");
		}
		
		// Trim array to exact size
		data = resize(data, count);
		
		return data;
	}
	
	public static WeatherRecord[] resize(WeatherRecord[] data, int size) {
		WeatherRecord[] temp = new WeatherRecord[size];
		int limit = data.length < size ? data.length: size;
		for (int i=0; i<limit; i++) {
			temp[i] = data[i];
		}
		
		return temp;
	}
	
	public static void menu(WeatherRecord[] data) {
		Scanner input = new Scanner(System.in);
		boolean done = false;
		
		do {
			System.out.println("1. Find the lowest low");
			System.out.println("2. Find the highest high");
			System.out.println("3. Find the average");
			System.out.println("4. Exit");
			System.out.print("Choice: ");
			String choice = input.nextLine();
			
			// menu
			switch (choice) {
				// 1. lowest low
				case "1":
					minimum(data);
					break;
				// 2. highest high
				case "2":
					maximum(data);
					break;
				// 3. average
				case "3":
					average(data);
					break;
				// 4. exit
				case "4":
					done = true;
					break;
				default:
					System.out.println("I'm sorry, but I can't do that.");
			}
			
		} while (!done);
		
		input.close();
	}
	
	public static void minimum(WeatherRecord[] data) {
		if (data.length == 0) {
			System.out.println("No data available.");
		} else {
			int m = 0;
			for (int i=1; i<data.length; i++) {
				if (data[i].getLow() < data[m].getLow()) {
					m = i;
				}
			}
			
			System.out.println("Lowest low was " + data[m].getLow() + " and occurred on " + data[m].getDate());
		}
	}
	
	public static void maximum(WeatherRecord[] data) {
		if (data.length == 0) {
			System.out.println("No data available.");
		} else {
			int m = 0;
			for (int i=1; i<data.length; i++) {
				if (data[i].getHigh() > data[m].getHigh()) {
					m = i;
				}
			}
			
			System.out.println("Highest high was " + data[m].getHigh() + " and occurred on " + data[m].getDate());
		}
	}
	
	public static void average(WeatherRecord[] data) {
		if (data.length == 0) {
			System.out.println("No data available.");
		} else {
			double total = 0;
			for (int i=0; i<data.length; i++) {
				double average = (data[i].getLow() + data[i].getHigh()) / 2.0;
				total = total + average;
			}
			double average = total / data.length;
			System.out.println("Average temperature was " + average);
		}
	}
	
	public static void main(String[] args) {
		// Read in data
		WeatherRecord[] data = readData("src/lectures/objects/weather.csv");
		menu(data);
		
		System.out.println("Program ending...");
	}

}
