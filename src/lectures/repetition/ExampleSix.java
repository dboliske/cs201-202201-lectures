package lectures.repetition;

public class ExampleSix {

	public static void main(String[] args) {
		int loopCount = 1; // initialize
		while (loopCount <= 10) { // condition
			System.out.println(loopCount);
			loopCount++; // update
		}
	}

}
