package lectures.repetition;

import java.util.Scanner;

public class ExampleOneB {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create scanner
		
		String value = "";
		
		do {
			System.out.println("Input: ");
			value = input.nextLine();
			if (!value.equalsIgnoreCase("done")) {
				System.out.println("Echo: " + value);
			}
		} while (!value.equalsIgnoreCase("done"));
		
		input.close();
		
		System.out.println("Goodbye");
	}

}
