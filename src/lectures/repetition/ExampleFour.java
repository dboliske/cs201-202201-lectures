package lectures.repetition;

import java.util.Scanner;

public class ExampleFour {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		boolean running = true; // initialize flag
		
		while(running) {
			// print user menu
			System.out.println("a. Print 'Apple'");
			System.out.println("b. Print 'Bat'");
			System.out.println("c. Print 'Cat'");
			System.out.println("d. Done, exit program.");
			System.out.print("Choice: ");
			String in = input.nextLine();
			
			// handle user menu option
			switch (in.toLowerCase()) {
				case "a":
					System.out.println("Apple");
					break;
				case "b":
					System.out.println("Bat");
					break;
				case "c":
					System.out.println("Cat");
					break;
				case "d":
					running = false; // change flag variable
					break;
				default:
					System.out.println("I'm sorry, but that's not an option.");
			}
		}
		
		input.close();
		
		System.out.println("Goodbye");
	}

}
