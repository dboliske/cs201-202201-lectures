package lectures.repetition;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class ExampleThree {

	public static void main(String[] args) throws IOException {
		File file = new File("src/lectures/repetition/weather.csv");
		Scanner input = new Scanner(file); // create scanner for file
		
		double lowest = 1000;
		
		while (input.hasNextLine()) {
			String line = input.nextLine(); // next line of data e.g. 2022-01-01,20,40
			String temps = line.substring(line.indexOf(',')+1); // read temperatures e.g. 20,40
			String min = temps.substring(0, temps.indexOf(',')); // get minimum 20
			
			double current = Double.parseDouble(min);
			if (current < lowest) {
				lowest = current;
			}
		}
		
		input.close();
		
		System.out.println("Minimum: " + lowest);
	}

}
