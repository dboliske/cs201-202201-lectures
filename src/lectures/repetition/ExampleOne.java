package lectures.repetition;

import java.util.Scanner;

public class ExampleOne {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create a scanner for user input
		
		System.out.print("Input: ");
		String value  = input.nextLine();
		
		while(!value.equalsIgnoreCase("done")) {
			System.out.println("Echo: " + value);
			System.out.print("Input: ");
			value  = input.nextLine();
		}
		
		input.close();
		
		System.out.println("Goodbye");
	}

}
