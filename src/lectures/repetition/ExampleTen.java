package lectures.repetition;

import java.util.Scanner;

public class ExampleTen {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Size: ");
		int n = Integer.parseInt(input.nextLine());
		input.close();
		
		for (int i=1; i<=n; i++) {
			for (int j=1; j<=n; j++) {
				int res = i * j;
				if (res >= 10) {
					System.out.print(" " + res);
				} else {
					System.out.print("  " + res);
				}
			}
			System.out.println(); // start next row
		}
	}

}
