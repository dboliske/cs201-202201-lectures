package lectures.repetition;

public class ExampleSeven {

	public static void main(String[] args) {
		int count = 1; // initialize
		do {
			System.out.println(count);
			count++; // update
		} while (count <= 10); // condition
	}

}
