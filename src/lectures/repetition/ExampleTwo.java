package lectures.repetition;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class ExampleTwo {

	public static void main(String[] args) throws IOException {
		File file = new File("src/lectures/repetition/data.txt");
		Scanner input = new Scanner(file); // read data from file
		
		while (input.hasNextLine()) {
			System.out.println(input.nextLine());
		}
		
		input.close();
	}

}
