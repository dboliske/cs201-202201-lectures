package lectures.repetition;

import java.util.Scanner;

public class ExampleFiveB {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // reading in user input
		
		System.out.println("1. Print 'Apple'");
		System.out.println("2. Print 'Bat'");
		System.out.println("3. Print 'Cat'");
		System.out.println("4. Exit");
		System.out.print("Choice: ");
		String choice = input.nextLine(); // get user's choice
		
		boolean done = choice.equals("4"); // flag control variable
		while (!done) {
			switch (choice) {
				case "1":
					System.out.println("Apple");
					break;
				case "2":
					System.out.println("Bat");
					break;
				case "3":
					System.out.println("Cat");
					break;
				case "4":
					done = true;
					break;
				default:
					System.out.println("That is not a valid choice.");
			}
			
			if (!done) {
				System.out.println("1. Print 'Apple'");
				System.out.println("2. Print 'Bat'");
				System.out.println("3. Print 'Cat'");
				System.out.println("4. Exit");
				System.out.print("Choice: ");
				choice = input.nextLine(); // get user's choice
			}
		}
		
		input.close();
		
		System.out.println("Goodbye!");
	}

}
