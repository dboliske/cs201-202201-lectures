package lectures.repetition;

import java.util.Scanner;

public class ExampleNine {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in); // get user input
		
		System.out.print("Size: ");
		int size = Integer.parseInt(input.nextLine());
		
		for (int row=0; row<size; row++) {
			for (int col=0; col<size; col++) {
				System.out.print("* ");
			}
			System.out.println(); 
		}
		
	}

}
